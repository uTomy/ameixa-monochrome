#!/bin/bash
# Thanks to Pander

if [ -e icons ]; then
    rm -rf icons
fi
cp -f ../ameixa/app/src/main/res/xml/drawable.xml app/src/main/res/xml
cp -f ../ameixa/app/src/main/res/xml/appfilter.xml app/src/main/res/xml
cp -f ../ameixa/app/src/main/res/values/iconpack.xml app/src/main/res/values
cp -r ../ameixa/icons .

cd icons
dark=424242

echo Before '<rect.*" fill="#......"/>'
grep '<rect.*" fill="#......"/>' *.svg|awk -F 'fill' '{print $2}'|sort|uniq -c|sort -nr
for i in *svg
do
    echo Monochroming ${i}...
    for col in 009688 00bcd4 03a9f4 2196f3 3f51b5 4caf50 607d8b 673ab7 795548 8bc34a 9c27b0 9e9e9e cddc39 e91e63 f44336 ff5722 ff9800 ffc107 ffeb3b
    do
        if [ `grep -hc '<rect.*" fill="#'${col} ${i}`'"/>' != 0 ]
        then
            sed -i -e 's/\(rect .*" fill="#\)'${col}'"/\1'${dark}'"/g' ${i}
        fi
    done
done

# hasta aqui funciona OK, revisar arriba los colores para arreglar fallo naranja

cd ..

if [ ! -e app/src/main/res/drawable-mdpi ]; then
    mkdir -p app/src/main/res/drawable-mdpi
fi
if [ ! -e app/src/main/res/drawable-hdpi ]; then
    mkdir -p app/src/main/res/drawable-hdpi
fi
if [ ! -e app/src/main/res/drawable-xhdpi ]; then
    mkdir -p app/src/main/res/drawable-xhdpi
fi
if [ ! -e app/src/main/res/drawable-xxhdpi ]; then
    mkdir -p app/src/main/res/drawable-xxhdpi
fi
if [ ! -e app/src/main/res/drawable-xxxhdpi ]; then
    mkdir -p app/src/main/res/drawable-xxxhdpi
fi

rm -f app/src/main/res/drawable-mdpi/*.svg
rm -f app/src/main/res/drawable-hdpi/*.svg
rm -f app/src/main/res/drawable-xhdpi/*.svg
rm -f app/src/main/res/drawable-xxhdpi/*.svg
rm -f app/src/main/res/drawable-xxxhdpi/*.svg

for SVG in icons/*.svg; do
	N=$(basename ${SVG} .svg)
    echo Exporting ${N}...
	inkscape -f ${SVG} -w 48 -h 48 -e \
		app/src/main/res/drawable-mdpi/${N}.png > /dev/null
	inkscape -f ${SVG} -w 72 -h 72 -e \
		app/src/main/res/drawable-hdpi/${N}.png > /dev/null
	inkscape -f ${SVG} -w 96 -h 96 -e \
		app/src/main/res/drawable-xhdpi/${N}.png > /dev/null
	inkscape -f ${SVG} -w 144 -h 144 -e \
		app/src/main/res/drawable-xxhdpi/${N}.png > /dev/null
	inkscape -f ${SVG} -w 192 -h 192 -e \
		app/src/main/res/drawable-xxxhdpi/${N}.png > /dev/null
done
